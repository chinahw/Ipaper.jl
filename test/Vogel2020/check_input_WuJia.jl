using nctools
using Ipaper
using NaNStatistics


f = "/share/Data/CN0.5.1_ChinaDaily_025x025/HI-Tmax_CN05.1_1961_2021_daily_025x025.nc"
lon = nc_read(f, "lon")
lat = nc_read(f, "lat")

dates = nc_date(f)
@time arr = nc_read(f);

size(arr)

nansum(x::AbstractVector) = sum(x .== -9999.0f0)
@time x = mapslices(nansum, arr, dims=3)[:, :, 1];

dates = nc_date(f)
# 45个网格缺测，435个站点；因此需要使用NanQuantile
22280 - 21845
inds = findall(@.(x != 22280 & x > 0))

r = map(I -> begin
    val = @view arr[I[1], I[2], :]
    _ind = val .!== -9999.0f0
    _dates = dates[_ind]
    _dates#[[1, end]]
  end, inds)


@time x1 = nc_read("OBS_mTRS_full_simple.nc"; ind=(:, :, :, 1));
@time x2 = nc_read("OBS_mTRS_full_simple_V2.nc", ind=(:, :, :, 1));

x1[x1.==-9999.0f0] .= NaN32;
x1 == x2

# 确实存在一定的差异，怀疑是`-9999f0`的影响

@time data1 = nc_read("OBS_mTRS_full_V2.nc"; ind=(:, :, :, 1));
@time data2 = nc_read("OBS_mTRS_full_simple_V2.nc", ind=(:, :, :, 1));

begin
  n, m, ntime = size(x1)
  by = 5
  ind = (1:by:n, 1:by:m, 1)
  _x1 = data1[ind...]
  _x2 = data2[ind...]

  ind_good = .!(isnan(_x1))
  n = sum(ind_good)
  
  diff = abs.(_x1 - _x2)
  n_good = sum(diff .<= 1.0)
  
  # 差别最大有3度
  # nanmaximum(abs.(_x1 - _x2))
end
