# julia --threads 16 test/Vogel2020/test_vogel2020.jl
@time using Ipaper

using nctools
using JLD2

function _ncwrite(mTRS_full, outfile="OBS_mTRS_full_V2.nc")
  _dims = nc_dims(f)
  probs = [0.9, 0.95, 0.99, 0.999, 0.9999]
  dim_prob = NcDim("prob", probs)
  dims = [_dims; dim_prob]

  @time nc_write(outfile, "mTRS", mTRS_full, dims; compress=1, overwrite=true)
  # jldsave("OBS_mTRS_full_V1.jld2"; mTRS_full, lon, lat, dates, compress=true)
end

dates = nc_date(f)
@time arr = nc_read(f);
@time arr[arr.==-9999.0f0] .= NaN32;

# Range(dates)
# size(arr)
# length(dates)
# dates[[1, end]]
# T_year = cal_mTRS_seasonal(arr, dates)

# `mTRS_full`最耗时
Threads.nthreads()

if false
  @time mTRS_full = cal_mTRS_full(arr, dates; use_mov=true, na_rm=true)
  _ncwrite(mTRS_full, "OBS_mTRS_full_V2.nc")
end

# 1675.866281 seconds (8.29 G allocations: 3.986 TiB, 24.24% gc time, 0.45% compilation time)
# 1376.109929 seconds (5.21 G allocations: 334.383 GiB, 24.46% gc time, 1.52% compilation time)
# 0.465 hours, 16 threads

# @profview_allocs mTRS_full = cal_mTRS_full(arr, dates; probs=[0.9]);
@time mTRS_full_simple = cal_mTRS_full(arr, dates; use_mov=false, na_rm=true);
_ncwrite(mTRS_full_simple, "OBS_mTRS_full_simple_V2.nc")

## 验收结果
# `full`和`simple`差距还是挺明显
